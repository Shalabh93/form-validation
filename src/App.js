import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import TextInputComp from './components/textInputComp'
import RadioInput from './components/radioInput'
import SelectComp from './components/selectComp'
import * as formAction from './actions/formAction'


class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selectData: [],
      radioData: [],
      emailData: [],
      telephoneData: [],
      hiddenData: [],
      formSubmitData: [],
      selectedValue: null
    }
    this.validateEmail = this.validateEmail.bind(this)
    this.validatePhone = this.validatePhone.bind(this)
    this.validateSelect = this.validateSelect.bind(this)
    this.validateRadio = this.validateRadio.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentWillMount(){
    this.props.getFormData();
  }

  validateEmail(label,req,email) {
    let valid, copyData = this.state.formSubmitData;
      var regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      valid = (!req && !email) || regex.test(String(email).toLowerCase());
      copyData.forEach(function(obj,index){
        if(obj['label'] === label){
          copyData.splice(index, 1)
        }
      })
      this.state.formSubmitData.push({
          label: label,
          isValid: valid,
          value: email
        })
    // console.log(this.state.formSubmitData);
  }

  validatePhone(label,req,number) {
    let copyData = this.state.formSubmitData;
    let valid = ((req && number.length === 10) ? true : false)
    copyData.forEach(function(obj,index){
      if(obj['label'] === label){
        copyData.splice(index, 1)
      }
    })
    this.state.formSubmitData.push({
      label: label,
      isValid: valid,
      value: number
    })
    // console.log(this.state.formSubmitData);
  }

  validateSelect(label,value) {
    let copyData = this.state.formSubmitData;
    copyData.forEach(function(obj,index){
      if(obj['label'] === label){
        copyData.splice(index, 1)
      }
    })
    this.state.formSubmitData.push({
      label: label,
      isValid: true,
      value: value
    })
    // console.log(this.state.formSubmitData);
  }

  validateRadio(label, value) {
    let setValid = value === '' ? false : true
    let copyData = this.state.formSubmitData;
    copyData.forEach(function(obj,index){
      if(obj['label'] === label){
        copyData.splice(index, 1)
      }
    })
    this.state.formSubmitData.push({
      label: label,
      isValid: setValid,
      value: value
    })
    // console.log(this.state.formSubmitData);
  }

  handleSubmit() {
    const data = JSON.stringify(this.state.formSubmitData) ;
    console.log('Submitted Data: ', data)
  }

  render() {
    const {formData} = this.props;
    let checkType
      formData.forEach((obj) => {
        checkType = obj.type
        switch(checkType) {
          case 'select':
            return this.state.selectData.push(obj)
          case 'radio':
            return this.state.radioData.push(obj)
          case 'email':
            return this.state.emailData.push(obj)
          case 'telephone':
            return this.state.telephoneData.push(obj)
          case 'hidden':
            return this.state.hiddenData.push(obj)
          default:
            return true
        }
      });
      if(formData.length !== 0) {
      return (
        <div>
          <TextInputComp type='email' data={this.state.emailData} validate={this.validateEmail} />
          <RadioInput data={this.state.radioData} validate={this.validateRadio} />
          <SelectComp data={this.state.selectData} validate={this.validateSelect} />
          <TextInputComp type='number' data={this.state.telephoneData} validate={this.validatePhone} />
          <input type='submit' value='Submit' onClick={this.handleSubmit}/>
        </div>
        );
    }
    return (<div />)
  }
  }

function mapStateToProps(state) {
  return {
    formData: state.formReducer.formData
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getFormData: formAction.getFormData
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
