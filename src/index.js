import React from 'react';
import { render } from 'react-dom';
import {Provider} from 'react-redux';
import { Router, Route, hashHistory} from 'react-router';

import './index.css';

import App from './App';
import configureStore from './store/configureStore';
import * as serviceWorker from './serviceWorker';

const store = configureStore();

render(
    <Provider store={store}>
        <Router history={hashHistory}>
            <Route path="/" component = {App} />
        </Router>
    </Provider>,
    document.getElementById('root')
);
serviceWorker.unregister();
