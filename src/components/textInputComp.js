import React, { Component } from 'react';
import './Application.css'
class TextInputComp extends Component {

    componentDidMount(){
        const {data} = this.props; 
        this.props.validate(data[0].label, !data[0].isOptional, '');
    }

    render() {
    return (
        <div>
        {this.props.data.map(item => {
            return (
                <div className='textinput'>
                <label clasName='textcolor'>{item.label}</label>
                <input style={{ padding: '5px 5px 5px 5px' }} type={this.props.type} required={item.isOptional ? !item.isOptional : true} onBlur={(event) => this.props.validate(item.label,event.target.required, event.target.value)}></input>
                </div>
            )
            })
        }
            </div>
        )
    }
}

export default TextInputComp;