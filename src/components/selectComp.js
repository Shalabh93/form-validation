import React, { Component } from 'react';

class SelectComp extends Component {

    componentDidMount(){
        const {data} = this.props; 
        this.props.validate(data[0].label, data[0].default);
    }

    render() {
    return (
    <div>
        {this.props.data.map(states => {
          return (
            <div>
              <label>{states.label}</label>
              <select onChange={(event)=>this.props.validate(states.label,event.target.value)}>
                {states.value.map(allStates => {
                  return (
                    states.default === allStates ? <option selected>{allStates}</option> : 
                    <option>{allStates}</option>
                  )
                })
              }
              </select>
            </div>
          )
        })}
    </div>
        )
    }
}

export default SelectComp;