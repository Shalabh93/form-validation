import React, { Component } from 'react';
import './Application.css'
class RadioInput extends Component {

    componentDidMount(){
        const {data} = this.props; 
        this.props.validate(data[0].label, '');
    }

    render() {
    return (
        <div>
        {this.props.data.map(item => {
          return (  
          <div>
            <label>{item.label}</label> 
            {item.value.map((gender,index) => {
              return(
                 <div>
                 <input key={index} type='radio' name='gender' value={gender} onChange={(event)=>this.props.validate(item.label,event.target.value)}/>{gender}
                 </div>
              )
            })}
          </div>
          )
        })}
        </div>
        )
    }
}

export default RadioInput;