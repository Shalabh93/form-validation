const initialState = {
    formData: []
}
export default function formReducer(state = initialState, action) {
    switch(action.type) {
        case 'FORM_DATA':
            return Object.assign([], state, {formData: action.data});
        default:
            return state;
    }
}
