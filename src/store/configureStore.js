/*
* Set up the redux store here, by making it aware of all reducers and specifying any middleware needed
*/
/* eslint-disable import/no-extraneous-dependencies */
import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import rootReducer from '../reducers/index';

//paramater to initialize state
export default function configureStore(initialState) {
	//optional 3rd parameter for middleware + redux testing
	if(process.env.NODE_ENV === 'production') {
		return createStore(
			rootReducer,
			initialState,
            compose(
                applyMiddleware(thunk)
            )
		);
	}
	else {
		return createStore(
			rootReducer,
			initialState,
			compose(
				applyMiddleware(reduxImmutableStateInvariant(), thunk),
				window.devToolsExtension ? window.devToolsExtension() : f => f
				)
		);
	}
}
