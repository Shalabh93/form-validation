export function setFormData(data){
  return {type: 'FORM_DATA', data: data};
}

export function getFormData(){
    return function(dispatch){
      /*axios.get('https://localhost:1337/https://ansible-template-engine.herokuapp.com/form',{
        mode: 'no-cors',
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Methods': '*'
        }
      })
	      .then((response) => {
          dispatch(setFormData(response.data.data));
	      })
	      .catch((err) => {
	        console.log(err);
          });*/
         var form = [{"label":"Email address","type":"email","isOptional":false,"isHidden":false},{"label":"Gender","type":"radio","value":["M (Male)","F (Female)","X (Indeterminate/Intersex/Unspecified)"],"isOptional":true},{"label":"State","type":"select","value":["NSW","QLD","SA","TAS","VIC","WA","NT","ACT"],"default":"ACT"},{"label":"Contact number","type":"telephone"},{"type":"hidden","value":1547279090913,"isHidden":true}] 
         dispatch(setFormData(form)); 
    }

}
